/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * REFERENCE
 * 
 * guide code used for food aid
 * https://www.coders-hub.com/2015/07/red-black-tree-rb-tree-using-c.html#.Wzu-knZKjQp
 * 
 * used support material
 * http://staff.ustc.edu.cn/~csli/graduate/algorithms/book6/chap14.htm
 */

/* 
 * File:   main.cpp
 * Author: vinicius
 *
 * Created on 18 de Junho de 2018, 14:23
 */

#include <iostream>
#include <cstdlib>

#include "RBTree.h"
#include "PrintColor.h"

using namespace std;

// menu method escope
int menu();

/*
 * 
 */
int main(int argc, char** argv) {
    system("clear");

    RBTree tree;

    Node *node_aux = NULL;
    int *arrayToInsert = NULL;
    int option;
    char flag;
    int key_aux;

    // 1º Caso de Teste
    tree.Insert(41);
    tree.Insert(38);
    tree.Insert(31);
    tree.Insert(12);
    tree.Insert(19);
    tree.Insert(8);

    // 2º Caso de Teste
    //    tree.Insert(50);
    //    tree.Insert(70);
    //    tree.Insert(90);
    //    tree.Insert(100);
    //    tree.Insert(95);
    //    tree.Insert(96);
    //    tree.Insert(98);
    //    tree.Insert(40);
    //    tree.Insert(45);
    //    tree.Insert(1);

    while (true) {

        option = menu();
        system("clear");

        switch (option) {

            case 1:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              SIZE               ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " ➸ SIZE: " << tree.Size() << endl;
                break;

            case 2:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              SEARCH             ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " ➸ Enter the key to search: ";
                cin >> key_aux;
                node_aux = tree.Search(key_aux);

                if (node_aux != NULL) {
                    cout << "\n ➸ SEARCH: Key found" << endl;

                    // Print Key
                    if (node_aux->color == RED) {
                        cout << "\t ➸ KEY \t\t " << ANSI_COLOR_RED << node_aux->key << ANSI_COLOR_RESET << endl;
                        cout << "\t ➸ COLOR \t " << ANSI_COLOR_RED << "RED(" << node_aux->color << ")" << ANSI_COLOR_RESET << endl;
                    } else {
                        cout << "\t ➸ KEY \t\t " << ANSI_COLOR_BLACK << node_aux->key << ANSI_COLOR_RESET << endl;
                        cout << "\t ➸ COLOR \t " << ANSI_COLOR_BLACK << "BLACK(" << node_aux->color << ")" << ANSI_COLOR_RESET << endl;
                    }

                    // Print Parent
                    if (!node_aux->parent)
                        cout << "\t ➸ PARENT \t " << "NULL" << endl;
                    else if (node_aux->parent->color == RED)
                        cout << "\t ➸ PARENT \t " << ANSI_COLOR_RED << node_aux->parent->key << ANSI_COLOR_RESET << endl;
                    else
                        cout << "\t ➸ PARENT \t " << ANSI_COLOR_BLACK << node_aux->parent->key << ANSI_COLOR_RESET << endl;

                    // Print Left
                    if (!node_aux->left)
                        cout << "\t ➸ LEFT \t " << "NULL" << endl;
                    else if (node_aux->left->color == RED)
                        cout << "\t ➸ LEFT \t " << ANSI_COLOR_RED << node_aux->left->key << ANSI_COLOR_RESET << endl;
                    else
                        cout << "\t ➸ LEFT \t " << ANSI_COLOR_BLACK << node_aux->left->key << ANSI_COLOR_RESET << endl;

                    // Print Right
                    if (!node_aux->right)
                        cout << "\t ➸ RIGHT \t " << "NULL" << endl;
                    else if (node_aux->right->color == RED)
                        cout << "\t ➸ RIGHT \t " << ANSI_COLOR_RED << node_aux->right->key << ANSI_COLOR_RESET << endl;
                    else
                        cout << "\t ➸ RIGHT \t " << ANSI_COLOR_BLACK << node_aux->right->key << ANSI_COLOR_RESET << endl;

                } else
                    cout << "\n ➸ SEARCH: Key not found" << endl;
                break;

            case 3:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              INSERT             ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " ➸ Enter the key to insert: ";
                cin >> key_aux;
                tree.Insert(key_aux);
                cout << "\n ➸ INSERT: Key " << key_aux << " inserted successfully" << endl;
                break;

            case 4:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              REMOVE             ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << " ➸ Enter the key: ";
                cin >> key_aux;

                if (tree.Remove(key_aux))
                    cout << "\n ➸ INSERT: Key removed successfully" << endl;
                else
                    cout << "\n ➸ INSERT: Key not found" << endl;
                break;

            case 5:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐              CLEAR             ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                tree.Clear();
                cout << "\n ➸ CLEAR: RedBlackTree cleans with success" << endl;
                break;

            case 6:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐          PRINT PRE-ORDER        ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "\n ➸ PRINT PRE-ORDER: ";
                tree.PrintPreOrder();
                cout << endl;
                break;

            case 7:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐            PRINT LEVEL          ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "\n ➸ PRINT LEVEL: ";
                tree.PrintLevel();
                cout << endl;
                break;

            case 8:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐          PRINT POS-ORDER        ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "\n ➸ PRINT POS-ORDER: ";
                tree.PrintPosOrder();
                cout << endl;
                break;

            case 9:
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐           PRINT IN-ORDER        ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "\n ➸ PRINT IN-ORDER: ";
                tree.PrintInOrder();
                cout << endl;
                break;

            case 10:
                int length, num;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "▐             RBTREEify           ▐" << endl;
                cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
                cout << "\n ➸ Enter the size of the array: ";
                cin >> length;
                cout << endl;

                arrayToInsert = new int[length];

                for (int i = 0; i < length; i++) {
                    cout << " ➸ array[" << i << "] = ";
                    cin >> num;
                    arrayToInsert[i] = num;
                }

                tree.RBTreeIfy(arrayToInsert, length);
                cout << "\n ➸ RBTREEify: Values inserted successfully" << endl;
                delete arrayToInsert;
                break;

            case 0:
                exit(0);
                break;

            default:
                cout << "\n ➸ Invalid option. PLease choose a valid option! ( 1 - 10 )" << endl;
                break;
        }

        cout << "\n\n ➸ Continue? (y / n):  ";
        cin >> flag;

        if (flag == 'n' || flag == 'N')
            exit(EXIT_SUCCESS);
    }

    return 0;
}

/**
 * @return which function of the RedBlackTree the user wants to call.
 */
int menu() {
    system("clear");

    int option = 0;

    cout << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐                          RED BLACK TREE                        ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐\t" << "                                                         ▐" << endl;
    cout << "▐\t" << "[  1  ] ➸ Size              [  6  ] ➸ PrintPreOrder      ▐" << endl;
    cout << "▐\t" << "[  2  ] ➸ Search            [  7  ] ➸ PrintLevel         ▐" << endl;
    cout << "▐\t" << "[  3  ] ➸ Insert            [  8  ] ➸ PrintPosOrder      ▐" << endl;
    cout << "▐\t" << "[  4  ] ➸ Remove            [  9  ] ➸ PrintInOrder       ▐" << endl;
    cout << "▐\t" << "[  5  ] ➸ Clear             [ 10  ] ➸ RBTreeIfy          ▐" << endl;
    cout << "▐\t" << "                                                         ▐" << endl;
    cout << "▐\t" << "                            (  0  ) ➸ EXIT               ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " \n ➸ Choose an option ➸  ";
    cin >> option;

    return option;
}

