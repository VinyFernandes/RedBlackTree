/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RedBlackTree.cpp
 * Author: vinicius
 * 
 * Created on 18 de Junho de 2018, 19:03
 */

#include <deque>
#include <stddef.h>

#include "RBTree.h"
#include "PrintColor.h"

/**
 * 
 */
RBTree::RBTree() {
    this->count = 0;
    this->root = NULL;
}

/**
 * 
 */
RBTree::~RBTree() {
    this->Clear();
}

/**
 * 
 */
void RBTree::Clear() {
    delete this->root;
    this->root = NULL;
}

/**
 * 
 * @param array
 * @param length
 */
void RBTree::RBTreeIfy(int array[], int length) {
    for (int i = 0; i < length; i++)
        this->Insert(array[i]);
}

/**
 * 
 * @return 
 */
int RBTree::Size() {
    return (this->count);
}

/**
 * 
 * @param key
 * @return 
 */
Node* RBTree::Search(int key) {
    return this->search(this->root, key);
}

/**
 * 
 * @param root
 * @param key
 * @return 
 */
Node* RBTree::search(Node *&root, int key) {

    Node* p = root;
    int found = 0;

    while (p != NULL && found == 0) {

        if (p->key == key)
            found = 1;
        if (found == 0) {
            if (p->key < key)
                p = p->right;
            else
                p = p->left;
        }
    }

    if (found == 0)
        return NULL; // Element Not Found
    else
        return p; // Element found
}

/**
 * 
 * @param key
 */
void RBTree::Insert(int key) {
    this->insert(this->root, key);
}

/**
 * 
 * @param root
 * @param key
 */
void RBTree::insert(Node *&root, int key) {

    Node *x = root;
    Node *y = NULL;
    Node *z = new Node(key);

    if (this->search(root, key))
        return;

    while (x != NULL) {

        y = x;

        if (z->key < x->key)
            x = x->left;
        else
            x = x->right;
    }

    z->parent = y;

    if (y == NULL)
        root = z;
    else if (z->key < y->key)
        y->left = z;
    else
        y->right = z;

    this->count++;
    this->insertFixUp(root, z);
}

/**
 * 
 * @param root
 * @param x
 */
void RBTree::insertFixUp(Node *&root, Node *x) {

    Node *y = NULL;

    while ((x->parent != NULL) && (x->parent->color == RED)) {

        if ((x->parent->parent->left != NULL) && (x->parent->key == x->parent->parent->left->key)) {

            if (x->parent->parent->right != NULL)
                y = x->parent->parent->right;

            if ((y != NULL) && (y->color == RED)) {

                x->parent->color = BLACK;
                y->color = BLACK;
                x->parent->parent->color = RED;

                if (x->parent->parent != NULL)
                    x = x->parent->parent;

            } else {

                if ((x->parent->right != NULL) && (x->key == x->parent->right->key)) {
                    x = x->parent;
                    this->leftRotate(root, x);
                }

                x->parent->color = BLACK;
                x->parent->parent->color = RED;

                this->rightRotate(root, x->parent->parent);
            }

        } else {

            if (x->parent->parent->left != NULL)
                y = x->parent->parent->left;

            if ((y != NULL) && (y->color == RED)) {

                x->parent->color = BLACK;
                y->color = BLACK;
                x->parent->parent->color = RED;

                if (x->parent->parent != NULL)
                    x = x->parent->parent;

            } else {

                if ((x->parent->left != NULL) && (x->key == x->parent->left->key)) {
                    x = x->parent;
                    this->rightRotate(root, x);
                }

                x->parent->color = BLACK;
                x->parent->parent->color = RED;

                this->leftRotate(root, x->parent->parent);
            }
        }

    } // End while
    root->color = BLACK;
}

/**
 * 
 * @param root
 * @param x
 */
void RBTree::leftRotate(Node *&root, Node *x) {

    if (x->right == NULL)
        return;
    else {

        Node *y = x->right;
        x->right = y->left;

        if (y->left != NULL)
            y->left->parent = x;

        y->parent = x->parent;

        if (x->parent == NULL)
            root = y;
        else if ((x->parent->left != NULL) && (x == x->parent->left))
            x->parent->left = y;
        else
            x->parent->right = y;

        y->left = x;
        x->parent = y;
    }

}

/**
 * 
 * @param root
 * @param y
 */
void RBTree::rightRotate(Node *&root, Node *y) {

    if (y->left == NULL)
        return;
    else {

        Node *x = y->left;
        y->left = x->right;

        if (x->right != NULL)
            x->right->parent = y;

        x->parent = y->parent;

        if (y->parent == NULL)
            root = x;
        else if ((y->parent->right != NULL) && (y == y->parent->right))
            y->parent->right = x;
        else
            y->parent->left = x;

        x->right = y;
        y->parent = x;
    }

}

/**
 * 
 * @param key
 * @return 
 */
bool RBTree::Remove(int key) {
    return this->remove(this->root, key);
}

/**
 * 
 * @param root
 * @param key
 * @return 
 */
bool RBTree::remove(Node *&root, int key) {

    Node *x = NULL;
    Node *y = NULL;
    Node *z = root;

    // RBTree is empty
    if ((z->left == NULL) && (z->right == NULL) && (z->key == key)) {
        root = NULL;
        return false;
    }

    while (z->key != key && z != NULL) {

        if (key < z->key)
            z = z->left;
        else
            z = z->right;

        if (z == NULL)
            return false;
    }

    if ((z->left == NULL) || (z->right == NULL))
        y = z;
    else
        y = getSucessor(z);

    if (y->left != NULL)
        x = y->left;
    else if (y->right != NULL)
        x = y->right;

    if ((x != NULL) && (y->parent != NULL))
        x->parent = y->parent;

    if ((y != NULL) && (x != NULL) && (y->parent == NULL))
        root = x;
    else if (y == y->parent->left)
        y->parent->left = x;
    else
        y->parent->right = x;

    if (y != z)
        z->key = y->key;

    if ((y != NULL) && (x != NULL) && (y->color == BLACK))
        this->removeFixUp(root, x);

    this->count--;
    return true;
}

/**
 * 
 * @param root
 * @param x
 */
void RBTree::removeFixUp(Node *&root, Node *&x) {

    while ((x != root) && (x->color == BLACK)) {

        Node *w = NULL;

        if ((x->parent->left != NULL) && (x == x->parent->left)) {

            w = x->parent->right;

            if ((w != NULL) && (w->color == RED)) {
                w->color = BLACK;
                x->parent->color = RED;
                this->leftRotate(root, x->parent);
                w = x->parent->right;
            }

            if ((w != NULL) && (w->left != NULL) && (w->right != NULL)
                    && (w->left->color == BLACK) && (w->right->color == BLACK)) {

                w->color = RED;
                x = x->parent;

            } else if ((w != NULL) && (w->right->color == BLACK)) {

                w->left->color = BLACK;
                w->color = RED;
                this->rightRotate(root, w);
                w = x->parent->right;
            }

            if (w != NULL) {
                w->color = x->parent->color;
                x->parent->color = BLACK;
                w->right->color = BLACK;
                this->leftRotate(root, x->parent);
                x = root;
            }

        } else if (x->parent != NULL) {

            w = x->parent->right;

            if ((w != NULL) && (w->color == RED)) {
                w->color = BLACK;
                x->parent->color = RED;
                this->leftRotate(root, x->parent);
                w = x->parent->left;
            }

            if ((w != NULL) && (w->right != NULL) && (w->left != NULL)
                    && (w->right->color == BLACK) && (w->left->color == BLACK)) {

                w->color = RED;
                x = x->parent;

            } else if ((w != NULL) && (w->left != NULL) && (w->left->color == BLACK)) {

                w->right->color = BLACK;
                w->color = RED;
                this->rightRotate(root, w);
                w = x->parent->left;
            }

            if (x->parent != NULL) {
                w->color = x->parent->color;
                x->parent->color = BLACK;
            }

            if (w->left != NULL)
                w->left->color = BLACK;

            if (x->parent != NULL)
                this->leftRotate(root, x->parent);

            x = root;
        }

    } // End while

    root->color = BLACK;
}

/**
 * 
 * @param node
 * @return 
 */
Node* RBTree::getSucessor(Node * node) {

    Node *y = NULL;

    if (node->left != NULL) {
        y = node->left;

        while (y->right != NULL)
            y = y->right;

    } else {
        y = node->right;

        while (y->left != NULL)
            y = y->left;
    }
    return y;
}

/**
 * 
 */
void RBTree::PrintPreOrder() {
    this->printPreOrder(this->root);
}

/**
 * 
 * @param root
 */
void RBTree::printPreOrder(Node *&root) {

    if (root == NULL)
        return;

    this->visit(root);
    this->printPreOrder(root->left);
    this->printPreOrder(root->right);
}

/**
 * 
 */
void RBTree::PrintLevel() {
    this->printLevel(this->root);
}

/**
 * 
 * @param root
 */
void RBTree::printLevel(Node *&root) {
    deque<Node*> fila;

    if (root == NULL)
        return;

    fila.push_back(root);
    while (!fila.empty()) {

        Node* no_atual = fila.front();
        fila.pop_front();
        this->visit(no_atual);

        if (no_atual->left != NULL)
            fila.push_back(no_atual->left);

        if (no_atual->right != NULL)
            fila.push_back(no_atual->right);
    }
}

/**
 * 
 */
void RBTree::PrintPosOrder() {
    this->printPosOrder(this->root);
}

/**
 * 
 * @param root
 */
void RBTree::printPosOrder(Node *&root) {

    if (root == NULL)
        return;

    this->printPosOrder(root->left);
    this->printPosOrder(root->right);
    this->visit(root);
}

/**
 * 
 */
void RBTree::PrintInOrder() {
    this->printInOrder(this->root);
}

/**
 * 
 * @param root
 */
void RBTree::printInOrder(Node *&root) {

    if (root == NULL)
        return;

    this->printInOrder(root->left);
    this->visit(root);
    this->printInOrder(root->right);
}

/**
 * 
 * @param node
 */
void RBTree::visit(Node *&node) {

    if (node->color == RED)
        cout << ANSI_COLOR_RED;
    else
        cout << ANSI_COLOR_BLACK;

    cout << node->key << ANSI_COLOR_MAGENTA << " - " << ANSI_COLOR_RESET;
}

