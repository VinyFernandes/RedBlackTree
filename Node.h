/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Node.h
 * Author: vinicius
 *
 * Created on 18 de Junho de 2018, 14:25
 */

#ifndef NODE_H
#define NODE_H

#include <stddef.h>

#include "Color.h"

/**
 * RedBlackTree Node
 * 
 * This class is implemented to work with keys of type int. However, this class
 * can be easily adapted to work with templates. Retained in this form only for
 * greater didactics.
 */
class Node {
public:

    /**
     * Default constructor
     */
    Node();

    /**
     * Constructs a new node from a key
     * @param key
     */
    Node(int key);

    /**
     * Default destructor
     */
    virtual ~Node();

    // key associated with the node
    int key;

    // Color the node
    Color color;

    // left subtree
    Node* left;

    // right subtree
    Node* right;

    // parent node
    Node* parent;

};

#endif /* NODE_H */

