/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Node.cpp
 * Author: vinicius
 *
 * Created on 18 de Junho de 2018, 14:25
 */

#include <stddef.h>

#include "Node.h"
#include "Color.h"

Node::Node() {
    this->key = 0;
    this->color = RED;
    this->left = this->right = this->parent = NULL;
}

Node::Node(int key) {
    this->key = key;
    this->color = RED;
    this->left = this->right = this->parent = NULL;
}

Node::~Node() {
    delete this->left;
    delete this->right;
    delete this->parent;
}

