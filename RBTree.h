/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RedBlackTree.h
 * Author: vinicius
 *
 * Created on 18 de Junho de 2018, 19:03
 */

#ifndef RBTREE_H
#define RBTREE_H

#include <iostream>

#include "Color.h"
#include "Node.h"

using namespace std;

/**
 * RedBlackTree Class
 * 
 * Provides methods for creating and manipulating RedBlack trees. Works with nodes
 * whose key is of type int, but the class could be Easily adapted to work with
 * templates. Kept in this form for a greater didactics.
 */
class RBTree {
private:

    // Tree root node
    Node* root;

    // Number of tree nodes
    int count;

public:

    /**
     * Default constructor
     */
    RBTree();

    /**
     * Default destructor
     */
    virtual ~RBTree();

    /**
     * Accepts an array of keys as input and returns an RBTree with its array
     * keys inserted
     * @param array[]
     * @param length
     * @return 
     */
    void RBTreeIfy(int array[], int length);

    /**
     * "Clean" the tree, making it empty
     */
    void Clear();

    /**
     * Returns the amount of nodes in the tree
     * @return count
     */
    int Size();

    /**
     * Check if a given element is present in the tree, returning a pointer
     * to the searched element, if it is found;
     * @param key
     * @return node found or null
     */
    Node* Search(int key);

    /**
     * Inserts an element into the RedBlackTree. 
     * Accepting a key as parameter.
     * @param key
     */
    void Insert(int key);

    /**
     * Removes an element from the red-black tree;
     * @param key
     * @return 
     */
    bool Remove(int key);

    /**
     * Prints on screen all the elements contained in the red-black tree, 
     * running them in preorder.
     */
    void PrintPreOrder();

    /**
     * Prints on screen all the elements contained in the red-black tree, 
     * traversing them by level.
     */
    void PrintLevel();

    /**
     * Prints on screen all the elements contained in the red-black tree, 
     * running them in posorder.
     */
    void PrintPosOrder();

    /**
     * Prints on screen all the elements contained in the red-black tree, 
     * running them in order.
     */
    void PrintInOrder();

protected:

    /**
     * Check if a given element is present in the tree, returning a pointer
     * to the searched element, if it is found;
     * @param root
     * @param key
     * @return 
     */
    Node* search(Node *&root, int key);

    /**
     * Private method that inserts an element in the red-black tree;
     * @param root
     * @param key
     */
    void insert(Node *&root, int key);

    /**
     * Fix the tree if some of the properties have been violated
     * @param root
     * @param x
     */
    void insertFixUp(Node *&root, Node *x);

    /**
     * Performs a simple rotation to the left
     * @param root
     * @param p
     */
    void leftRotate(Node *&root, Node *node);

    /**
     * Performs a simple rotation to the right
     * @param root
     * @param p
     */
    void rightRotate(Node *&root, Node *node);

    /**
     * Removes an element from the red-black tree;
     * @param root
     * @param key
     * @return 
     */
    bool remove(Node *&root, int key);

    /**
     * Repair the tree, after removing a node, if necessary.
     * @param root
     * @param x
     */
    void removeFixUp(Node *&root, Node *&x);

    /**
     * Returns the successor node of the node that was passed as a parameter
     * @param node
     * @return 
     */
    Node* getSucessor(Node *node);

    /**
     * Prints on the screen all the elements contained in the Red Black * tree,
     * from a root node. Printing the keys in pre order.
     * @param root
     */
    void printPreOrder(Node *&root);

    /**
     * Prints on the screen all the elements contained in the Red Black * tree,
     * from a root node. Printing the keys in level
     * @param root
     */
    void printLevel(Node *&root);

    /**
     * Prints on the screen all the elements contained in the Red Black * tree,
     * from a root node. Printing the keys in pos order.
     * @param root
     */
    void printPosOrder(Node *&root);

    /**
     * Prints on the screen all the elements contained in the Red Black * tree,
     * from a root node. Printing the keys in order.
     * @param root
     */
    void printInOrder(Node *&root);

    /**
     * Prints the value of a node
     * @param node
     */
    void visit(Node *&node);

};

#endif /* RBTREE_H */

